import React from "react";
import "./index.css";

import UserContextProvider from "./context/userContext";
import SearchUser from "./components/SearchUser";
import UsersData from "./components/UsersData";

const App = () => {
  return (
    <UserContextProvider>
      <div className="app">
        <div className="app-header">
          <h1>Contacts</h1>
        </div>
        <SearchUser />
        <UsersData />
      </div>
    </UserContextProvider>
  );
};

export default App;
