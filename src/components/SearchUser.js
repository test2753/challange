import React, { useContext, useState } from "react";
import "../index.css";
import SearchIcon from "@material-ui/icons/Search";
import { UserContext } from "../context/userContext";

const SearchUser = () => {
  const { userData, setUserData } = useContext(UserContext);
  const [inputValue, setInputValue] = useState("");

  function onInputChange(e) {
    setInputValue(e.target.value);

    const filtredUser = userData.filter((user) => {
      return (
        user.first_name.toLowerCase().includes(inputValue) ||
        user.last_name.toLowerCase().includes(inputValue)
      );

      console.log(filtredUser);
    });
  }

  return (
    <div className="input-container">
      <SearchIcon className="search-button" />
      <input
        className="search-input"
        value={inputValue}
        onChange={onInputChange}
      />
    </div>
  );
};

export default SearchUser;
