import React, { useEffect, useState, useContext } from "react";
import { fetchUser } from "../request";
import PersonIcon from "@material-ui/icons/Person";
import Checkbox from "@material-ui/core/Checkbox";
import { UserContext } from "../context/userContext";

const UsersData = () => {
  const [users, setUsers] = useState([]);
  const [checked, setChecked] = useState(false);
  const { userData, setUserData } = useContext(UserContext);

  const handleChange = (id) => {
    setChecked(!checked);
    console.log(id);
  };

  useEffect(() => {
    async function getUser() {
      const data = await fetchUser();
      setUserData(data);

      data.sort((a, b) => {
        if (a.last_name < b.last_name) {
          return -1;
        }
        if (a.last_name > b.last_name) {
          return 1;
        }
        return 0;
      });

      const userData = data.map((user) => {
        return (
          <div key={user.id} className="users">
            <div>
              <div className="user-avatar-container">
                {user.avatar ? (
                  <img className="user-avatar" src={`${user.avatar}`} />
                ) : (
                  <div className="user-avatar-container ">
                    <PersonIcon className="user-avatar user-icon" />
                  </div>
                )}
              </div>
              <div className="user-name">
                {user.first_name} {user.last_name}
              </div>
            </div>
            <div>
              <div className="user-checkbox">
                <Checkbox
                  color="primary"
                  onChange={() => handleChange(user.id)}
                  inputProps={{ "aria-label": "secondary checkbox" }}
                />
              </div>
            </div>
          </div>
        );
      });
      setUsers(userData);
    }

    getUser();
  }, []);

  return <div>{users}</div>;
};

export default UsersData;
