const { createProxyMiddleware } = require("http-proxy-middleware");
module.exports = function (app) {
  app.use(
    ["/api"],
    createProxyMiddleware({
      target:
        "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json",
    })
  );
};
