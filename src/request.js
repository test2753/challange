const data_URL =
  "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json";
const cors_URL = "https://cors-anywhere.herokuapp.com/";

export async function fetchUser() {
  const res = await fetch(`${cors_URL}${data_URL}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });

  const data = await res.json();

  return data;
}
