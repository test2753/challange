1. Go to https://gitlab.com/test2753/challange and click "clone" button (copy URL - clone with HTTPS)
2. Go to code editor e.g VSC. 
3. Create new folder and initialize empty git repository with command git init.
4. Then clone repository with command git clone (git clone https://gitlab.com/test2753/challange.git)
5. After this install all project dependencies (npm install).
6. Go to https://cors-anywhere.herokuapp.com/corsdemo and click "Request temporary access to the demo server".
6. Type npm start and run project
